import os

import yaml


class ConfigError(ValueError):
    pass


class Settings:
    def __init__(self):
        file_path = os.path.join(os.path.dirname(__file__), 'data', 'settings.yaml')
        try:
            with open(file_path) as f:
                self.data = yaml.safe_load(f)
        except FileNotFoundError:
            raise ConfigError('No settings.yaml-file found!')

    def get(self, node_path) -> str:
        node = self.data
        for child_path in node_path.split('.'):
            node = node.get(child_path, {})

        if type(node) not in (str, dict, int):
            raise ConfigError('The requested node `%s` was not found in the settings.yaml-file!' % node_path)

        return node
