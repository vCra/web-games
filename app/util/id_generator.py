import time
from base64 import urlsafe_b64encode
from math import ceil


def generate_id(base_id: str) -> str:
    """
    Generate a unique id based on a unique base id and the current time, encoded in URL-safe Base64.
    :param base_id: the base id to derive the new id from
    :return: the new unique id
    """

    id_int = int('%s%i' % (base_id, time.time()))

    id_int_byte_length = int(ceil(id_int.bit_length() / 8))
    id_bytes = id_int.to_bytes(id_int_byte_length, 'big')

    id_str = urlsafe_b64encode(id_bytes).decode("utf-8")

    return id_str
