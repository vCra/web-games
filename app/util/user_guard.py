from functools import wraps

from flask import url_for, session, request
from flask_dance.contrib.discord import discord
from mongoengine import DoesNotExist
from werkzeug.utils import redirect

from app.model.user import User
from .common_constants import SESSION_DISCORD_ID_KEY, SESSION_TARGET_AFTER_AUTH
from .error.user_not_found_error import UserNotFoundError


def ensure_user_is_ready_for_http(base_function):
    def redirect_to_discord_id():
        session[SESSION_TARGET_AFTER_AUTH] = request.url
        return redirect(url_for('discord.login'))

    @wraps(base_function)
    def wrapper(*args, **kwargs):
        if not discord.authorized:
            return redirect_to_discord_id()

        try:
            user = get_user_from_session()
        except UserNotFoundError:
            return redirect_to_discord_id()

        return base_function(user, *args, **kwargs)

    return wrapper


def ensure_user_is_ready_for_ws(base_function):
    @wraps(base_function)
    def wrapper(*args, **kwargs):
        try:
            user = get_user_from_session()
        except UserNotFoundError:
            raise ConnectionRefusedError('no user found in session')

        return base_function(user, *args, **kwargs)

    return wrapper


def get_user_from_session() -> User:
    discord_id = session.get(SESSION_DISCORD_ID_KEY)
    if discord_id is None:
        raise UserNotFoundError('no discord_id in session')

    try:
        user = User.objects.get(discord_id=discord_id)
    except DoesNotExist:
        raise UserNotFoundError(f'no user found for discord_id {discord_id}')

    try:
        from sentry_sdk import configure_scope
        with configure_scope() as scope:
            # noinspection PyDunderSlots,PyUnresolvedReferences
            scope.user = {"username": f'{user.name} # {user.discord_id}'}
    except ModuleNotFoundError:
        pass  # ignore if sentry SDK is not installed

    return user
