from flask import request


def get_locale_from_request() -> str:
    return request.accept_languages.best_match(['en', 'de'])
