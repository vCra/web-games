def print_list_indented(obj_list: list, header: str = None, depth: int = 1):
    prefix = '\n' + ('    ' * depth)
    obj_str = prefix + prefix.join(obj_list)

    if header:
        print(header + obj_str)
    else:
        print(obj_str[1:])  # strip leading newline


def print_hr():
    print('\n' + ('-' * 80))
