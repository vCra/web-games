from datetime import datetime

from mongoengine import Document, ReferenceField, ListField, IntField, DateTimeField

from .user import User


class PlayerResult(Document):
    player = ReferenceField(User, required=True)
    points_history = ListField(IntField())
    created = DateTimeField(default=datetime.utcnow)

    meta = {
        'indexes': [
            {'fields': ['created'], 'cls': False, 'expireAfterSeconds': 48 * 60 * 60}
        ]
    }
