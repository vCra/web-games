### Credit for the decks
- The files starting with `CAH_` are original
    ***Cards Against Humanity*** game-decks. So we were free to rip them
    under CC-BY-2.0 as stated on the [CAH-Website](https://www.cardsagainsthumanity.com/).


### Adding other decks

The loading logic scans this folder for `.yaml` files.
So just add them here.


### Deck-File-Structure

```yaml
name: "some name"

questions:
  - "one blank: _"
  - "two blanks: _ _"
  - "no blank?"
  - "some \"weird\" formatting"
answers:
  - "an answer"
  - "another answer"
```

Every `_` is regarded as one blank. If none are present, one blank is assumed.
