import logging
from random import shuffle
from typing import Union

from mongoengine import IntField, EmbeddedDocumentListField, ListField, ReferenceField, EmbeddedDocumentField
from werkzeug.datastructures import ImmutableMultiDict

from app.model.user import User
from app.util.error.bad_user_request_error import BadUserRequestError
from app.util.mongoengine_fields import StringEnumField
from . import decks_service
from .model.cah_player_state import CahPlayerState
from .model.card import Card
from .model.cah_game_state import CahGameState
from ..base.base_match import BaseMatch


class CahMatch(BaseMatch):

    # region Fields
    points_to_win = IntField(required=True)
    hand_size = IntField(required=True)
    blank_cards_count = IntField(required=True)
    game_state = StringEnumField(CahGameState, required=True)
    player_states = ListField(ReferenceField('CahPlayerState'))

    current_round_player_states = ListField(ReferenceField('CahPlayerState'))
    """
    List of players who are playing cards in the current round
    """

    black_cards_stack = EmbeddedDocumentListField(Card)
    """
    The stack of black cards to be yet drawn
    """
    black_cards_pile = EmbeddedDocumentListField(Card)
    """
    The black cards thrown away or played previously
    """
    current_black_card = EmbeddedDocumentField(Card)
    white_cards_stack = EmbeddedDocumentListField(Card)
    """
    The stack of white cards to be yet drawn
    """
    white_cards_pile = EmbeddedDocumentListField(Card)
    """
    The white cards thrown away or played previously
    """

    current_czar = ReferenceField(User)
    winning_card_no = IntField()
    # endregion

    # region Constructor
    def _gl_init(self, **kwargs) -> None:
        decks = [decks_service.find_and_load_deck(name) for name in kwargs['selected_decks_names']]
        self.black_cards_stack = [c for d in decks for c in d.black_cards]
        shuffle(self.black_cards_stack)
        self.white_cards_stack = [c for d in decks for c in d.white_cards]
        shuffle(self.white_cards_stack)

        self.points_to_win = kwargs['points_to_win']
        self.blank_cards_count = kwargs['blank_cards_count']
        self.hand_size = 10  # TODO: add to UI
        self.game_state = CahGameState.STARTING
    # endregion

    # region Event Handlers
    def _gl_handle_join_init_player_state(self, user: User):
        return CahPlayerState(remaining_blank_cards=self.blank_cards_count)

    def _gl_handle_join(self, user: User) -> None:
        pass

    def _gl_handle_rejoin(self, user: User, player_state: CahPlayerState) -> None:
        pass

    def _gl_handle_disconnect(self, user: User) -> None:
        if user in self.current_round_player_states:
            self.modify(pull__current_round_player_states=user)
            self.__check_players_selection_finished()

        if user == self.current_czar:
            self.__advance_player_results()
            self._advance_round_number()
            self.modify(set__current_czar=None)
            self.__start_new_round()

    def _gl_handle_play(self, user: User, data: ImmutableMultiDict) -> None:
        action = data.get('action')
        if action is None:
            raise BadUserRequestError('no action')

        player_state = self._get_player_state(user)

        def __get_selected_card():
            card_no = data.get('card_no')
            if card_no is None:
                raise BadUserRequestError('invalid data (card_no)')
            try:
                return player_state.hand[int(card_no)]
            except ValueError or IndexError:
                raise BadUserRequestError('invalid data (c.no n/a)')

        if action == 'start':
            self.__start_new_round()
            self._emit_ws_state_change()

        elif action == 'player_select':
            if self.game_state != CahGameState.PLAYERS_PICKING or player_state is None:
                raise BadUserRequestError('not in select state')

            selected_card = __get_selected_card()
            self.__update_selected_card(player_state, selected_card)

        elif action == 'player_blank':
            if player_state.remaining_blank_cards == 0:
                raise BadUserRequestError('no blank cards left')

            value = data.get('value').strip()
            if len(value) > 0:
                new_card = Card.generate(value, True)
                player_state.update(set__remaining_blank_cards=player_state.remaining_blank_cards-1)
                self.__update_selected_card(player_state, new_card)
            else:
                self._emit_ws_state_change(user, player_state)  # refresh view to hide modal

        elif action == 'player_throw':
            if player_state is None or player_state.selected_card is None:
                raise BadUserRequestError('invalid state (selected_card n/a)')

            selected_card = __get_selected_card()
            self.modify(push__white_cards_pile=selected_card)
            player_state.modify(pull__hand=selected_card)
            self._emit_ws_state_change(user, player_state)

        elif action == 'player_throw_all':
            if player_state is None or player_state.selected_card is None:
                raise BadUserRequestError('invalid state (selected_card n/a)')

            for c in player_state.hand:
                self.modify(push__white_cards_pile=c)
            player_state.modify(set__hand=[])
            self._emit_ws_state_change(user, player_state)

        elif action in ['czar_select', 'czar_select_none']:
            if self.current_czar != user:
                raise BadUserRequestError('not czar')

            winning_player = None
            if not action.endswith('_none'):
                winning_card_no = data.get('card_no')
                if winning_card_no is None:
                    raise BadUserRequestError('invalid data (card_no)')
                try:
                    winning_player = self.current_round_player_states[int(winning_card_no)].player
                except ValueError or IndexError:
                    raise BadUserRequestError('invalid data (c.no n/a)')
                self.modify(set__winning_card_no=winning_card_no)

            self.__advance_player_results(winning_player)

            self.modify(set__game_state=CahGameState.ROUND_RESULT)
            self._emit_ws_player_change()  # for updated scores
            self._emit_ws_state_change()  # for updating the state for all

        elif action == 'czar_finish':
            if len([r for r in self.results if r.points_history[-1] == self.points_to_win]) > 0:
                sorted_results = sorted(self.results, key=lambda res: res.points_history[-1], reverse=True)
                self.modify(set__results=sorted_results)
                self.modify(set__game_state=CahGameState.FINISHED)
                self._emit_ws_finished()

            else:
                self.modify(inc__current_round_number=1)
                self.modify(push__black_cards_pile=self.current_black_card)
                self._advance_round_number()
                self.__start_new_round()

        else:
            raise BadUserRequestError('invalid action')

    def __update_selected_card(self, player_state: CahPlayerState, selected_card: Card):
        if player_state.selected_card:
            player_state.modify(push__hand=player_state.selected_card)

        player_state.modify(set__selected_card=selected_card)
        player_state.modify(pull__hand=selected_card)

        self._emit_ws_player_change()  # show selection status
        self._emit_ws_state_change(player_state.player, player_state)  # only update display for the affected player

        self.__check_players_selection_finished()

    def __start_new_round(self):
        if len(self.players) < 2:
            logging.info('Too few players, reverting to starting state')
            self.modify(set__game_state=CahGameState.STARTING)

        else:
            new_czar_index = self._find_next_czar_index()
            self.modify(set__current_czar=self.players[new_czar_index])

            round_players = self.players[0 : new_czar_index] + self.players[new_czar_index+1 : len(self.players)]
            round_ps = [ps for ps in self.player_states if ps.player in round_players]
            shuffle(round_ps)
            self.modify(set__current_round_player_states=round_ps)

            self.modify(set__winning_card_no=None)

            for state in round_ps:
                state.modify(set__selected_card=None)
                for i in range(len(state.hand), self.hand_size):
                    state.modify(push__hand=self.__get_next_card('white'))

            self.modify(set__current_black_card=self.__get_next_card('black'))

            self.modify(set__game_state=CahGameState.PLAYERS_PICKING)

        self._emit_ws_player_change()  # to reset the tags
        self._emit_ws_state_change()  # to notify all of the new state

    def _find_next_czar_index(self):
        if self.current_czar is None:
            new_czar_index = 0
        else:
            current_czar_index = self.players.index(self.current_czar)
            new_czar_index = current_czar_index + 1
            if new_czar_index >= len(self.players):
                new_czar_index = 0

        return new_czar_index

    def __check_players_selection_finished(self) -> None:
        did_all_select = True
        for player_state in self.current_round_player_states:
            if player_state.selected_card is None:
                did_all_select = False
                break

        if did_all_select:
            self.modify(set__game_state=CahGameState.CZAR_PICKING)
            self._emit_ws_state_change()  # update all

    def __advance_player_results(self, winning_player: User = None):
        if winning_player is not None:
            winning_pr = next(r for r in self.results if r.player == winning_player)
            winning_pr.modify(push__points_history=winning_pr.points_history[-1] + 1)
            stale_results = [r for r in self.results if r.player != winning_player]
        else:
            stale_results = self.results

        for result in stale_results:
            result.modify(push__points_history=result.points_history[-1])

        self.save()
    # endregion

    # region View Renderers
    def _gl_render_inner_view_for_state(self, user: User, player_state: CahPlayerState = None) -> Union[str, dict]:
        if self.game_state == CahGameState.STARTING:
            user_is_initiator = user.discord_id == self.initiator.discord_id
            return {
                'player': self._render_base_state_template(
                    'starting', user, user_is_initiator=user_is_initiator
                )
            }

        is_player_in_current_round = user in [ps.player for ps in self.current_round_player_states]
        if player_state is None:
            player_state = self._get_player_state(user) if is_player_in_current_round else None

        if self.game_state == CahGameState.PLAYERS_PICKING:
            if is_player_in_current_round:
                return {
                    'blackCard': self._render_state_template(
                        'black_card', user, card=self.current_black_card
                    ),
                    'player': self._render_state_template(
                        'players_picking', user, player_state=player_state
                    ),
                    'czar': self._render_state_template(
                        'czar_placeholder', user
                    ),
                }
            else:
                return {
                    'blackCard': self._render_state_template(
                        'black_card', user, card=self.current_black_card
                    ),
                    'player': self._render_base_state_template(
                        'empty', user
                    ),
                    'czar': self._render_state_template(
                        'czar_waiting', user
                    ),
                }

        is_czar = self.current_czar == user

        if self.game_state == CahGameState.CZAR_PICKING:
            return {
                'blackCard': self._render_state_template(
                    'black_card', user, card=self.current_black_card
                ),
                'czar': self._render_state_template(
                    'czar_picking', user, is_czar=is_czar, czar_color=self.current_czar.color,
                    round_player_states=self.current_round_player_states
                ),
                'player': self._render_state_template(
                    'players_waiting', user, player_state=player_state
                ),
            }

        if self.game_state == CahGameState.ROUND_RESULT:
            return {
                'blackCard': self._render_state_template(
                    'black_card', user, card=self.current_black_card
                ),
                'czar': self._render_state_template(
                    'czar_result', user, is_czar=is_czar, czar_color=self.current_czar.color,
                    round_player_states=self.current_round_player_states, winning_card_no=self.winning_card_no
                ),
                'player': self._render_base_state_template(
                    'empty', user
                )
            }

        raise RuntimeError(f'cannot handle state `{self.game_state}`')

    def _gl_get_player_tags(self, user: User) -> [str]:
        if self.current_czar == user:
            return ['gavel']

        if self.game_state != CahGameState.PLAYERS_PICKING:
            return []

        player_state = self._get_player_state(user)
        if player_state in self.current_round_player_states:
            if player_state.selected_card is not None:
                return ['check']
        else:
            return ['user-ninja']

        return []
    # endregion

    # region Getters
    @classmethod
    def get_internal_name(cls) -> str:
        return 'cah'
    # endregion

    # region Internal Helpers
    def __get_next_card(self, kind: str):
        modifiers = {}
        if len(self[f'{kind}_cards_stack']) == 0:
            shuffle(self[f'{kind}_cards_pile'])
            card = self[f'{kind}_cards_pile'][0]
            modifiers[f'set__{kind}_cards_stack'] = self[f'{kind}_cards_pile'][1:]
            modifiers[f'set__{kind}_cards_pile'] = []
        else:
            card = self[f'{kind}_cards_stack'][0]
            modifiers[f'pull__{kind}_cards_stack'] = card

        self.modify(**modifiers)

        return card
    # endregion
