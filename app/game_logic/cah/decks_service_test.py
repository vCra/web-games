import unittest

from app.util.debug import print_list_indented, print_hr
from .decks_service import list_decks, find_and_load_deck


class DecksServiceTestCase(unittest.TestCase):
    def test_list_decks(self):
        print_hr()

        result = list_decks()
        self.assertGreater(len(result), 0)

        print_list_indented([f'{deck.file_name} : {deck.name}' for deck in result], '>>> list_decks():')

    def test_load_deck_all(self):
        print_hr()

        deck_fns = [deck.file_name for deck in list_decks()]
        self.assertGreater(len(deck_fns), 0)

        for file_name in deck_fns:
            print(f">>> find_and_load_deck('{file_name}')")
            result = find_and_load_deck(file_name)
            self.assertIsInstance(result.black_cards, list)
            self.assertIsInstance(result.white_cards, list)

            print('    >>> black cards:')
            print_list_indented([card.text for card in result.black_cards], depth=2)
            print('    >>> white cards:')
            print_list_indented([card.text for card in result.white_cards], depth=2)


if __name__ == '__main__':
    unittest.main()
