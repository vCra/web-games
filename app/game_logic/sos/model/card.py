from mongoengine import EmbeddedDocument, BooleanField, StringField


class Card(EmbeddedDocument):
    round_on_me = BooleanField(required=True)
    text_a = StringField(required=True)
    text_b = StringField()
    colour = StringField(required=True)
