from flask_babel import _
from werkzeug.datastructures import ImmutableMultiDict

from app.model.user import User
from app.util.error.bad_user_request_error import BadUserRequestError
from . import decks_service
from .sos_match import SosMatch
from ..base.base_game import BaseGame


class SosGame(BaseGame):
    @classmethod
    def get_internal_name(cls) -> str:
        return 'sos'

    @classmethod
    def get_long_name(cls) -> str:
        return 'Spillage or Shots'

    @classmethod
    def get_supported_locales(cls) -> [str]:
        return ['en']

    @classmethod
    def render_view_new(cls, user: User):
        return cls._render_game_template('new', user=user, decks=decks_service.list_decks())

    @classmethod
    def init_new_match(cls, data: ImmutableMultiDict, initiator: User):
        selected_decks_indices = data.getlist('selectedDecks', int)
        if len(selected_decks_indices) < 1:
            raise BadUserRequestError(_('select_at_least_one_deck'))

        all_decks = decks_service.load_decks()
        selected_cards = []
        for i, deck_cards in enumerate(all_decks):
            if i in selected_decks_indices:
                selected_cards += deck_cards

        return SosMatch.init_new_match(
            initiator,
            cards=selected_cards
        )
