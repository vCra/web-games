import logging
import os

import yaml

from .model.card import Card
from .model.deck import Deck

file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'decks.yaml')


def list_decks() -> [Deck]:
    decks_list = []
    try:
        with open(file_path) as f:
            data = yaml.safe_load(f)

        for deck in data.get('decks', []):
            name = deck.get('name')
            if name is None:
                raise RuntimeError('missing name in deck-definition')
            colour = deck.get('colour')
            if colour is None:
                raise RuntimeError('missing colour in deck-definition')
            decks_list.append(Deck(name, colour))

    except BaseException as ex:
        logging.error(ex)
        raise RuntimeError('Failed when listing decks')

    return decks_list


def load_decks() -> [[Card]]:
    cards_per_deck = []
    try:
        with open(file_path) as f:
            data = yaml.safe_load(f)

        for deck in data.get('decks', []):
            cards = []
            colour = deck.get('colour')

            for card in deck.get('cards'):
                round_on_me = card.get('round_on_me', False)
                if not round_on_me:
                    text_a = card.get('textA')
                    text_b = card.get('textB')
                    if text_a is None or text_b is None:
                        raise RuntimeError('Missing text for card')
                    cards.append(Card(round_on_me=False, text_a=text_a, text_b=text_b, colour=colour))

                else:
                    text = card.get('text')
                    if text is None:
                        raise RuntimeError('Missing text for card')
                    cards.append(Card(round_on_me=True, text_a=text, colour=colour))

            cards_per_deck.append(cards)

    except BaseException as ex:
        logging.error(ex)
        raise RuntimeError('Failed when listing decks')

    return cards_per_deck
