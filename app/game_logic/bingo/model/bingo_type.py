from enum import Enum


class BingoType(Enum):
    UK = 'UK'
    US = 'US'
