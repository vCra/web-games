from mongoengine import ListField, DynamicField, BooleanField

from app.game_logic.base.model.base_player_state import BasePlayerState


class BingoPlayerState(BasePlayerState):
    card = ListField(ListField(DynamicField()))
    """
    A two dimensional array, representing the numbers and blanks on the player's card
    """
    marked = ListField(ListField(BooleanField(default=False)))
    """
    A two dimensional array, representing what fields are marked already
    """
