from typing import Union

from mongoengine import EmbeddedDocumentListField, IntField, ListField, ReferenceField
from werkzeug.datastructures import ImmutableMultiDict

from app.game_logic.bingo.number_service import shuffle_call_numbers
from app.model.user import User
from app.util.error.bad_user_request_error import BadUserRequestError
from app.util.mongoengine_fields import StringEnumField
from .abstract_bingo_service import AbstractBingoService
from .model.bingo_game_state import BingoGameState
from .model.bingo_player_state import BingoPlayerState
from .model.bingo_type import BingoType
from .model.number import Number
from .uk_bingo_service import UkBingoService
from .us_bingo_service import UsBingoService
from ..base.base_match import BaseMatch
from ..base.model.base_player_state import BasePlayerState


class BingoMatch(BaseMatch):

    # region Fields
    game_state = StringEnumField(BingoGameState, required=True)
    player_states = ListField(ReferenceField('BingoPlayerState'))

    type = StringEnumField(BingoType, required=True)
    numbers = EmbeddedDocumentListField(Number)
    call_numbers = ListField(IntField(min_value=1))

    winner = ReferenceField(User)
    # endregion

    # region Class Methods
    @classmethod
    def get_internal_name(cls) -> str:
        return 'bingo'
    # endregion

    # region Constructor
    def _gl_init(self, **kwargs) -> None:
        self.type = kwargs['type']
        self.numbers = kwargs['numbers']

        self.call_numbers = shuffle_call_numbers(self.__get_bingo_service().get_max_number())

        self.game_state = BingoGameState.STARTING
    # endregion

    # region Event Handlers
    def _gl_handle_join_init_player_state(self, user: User) -> BingoPlayerState:
        ps = BingoPlayerState()
        return self.__init_player_state(ps)

    def __init_player_state(self, ps: BingoPlayerState, is_reinit=False) -> BingoPlayerState:
        bingo_service = self.__get_bingo_service()
        ps.card = bingo_service.generate_card()
        (row_length, row_count) = bingo_service.get_card_size()
        ps.marked = [
            [False for _ in range(row_length)]
            for _ in range(row_count)
        ]
        if is_reinit:
            ps.save()
        return ps

    def _gl_handle_join(self, user: User) -> None:
        pass

    def _gl_handle_disconnect(self, user: User) -> None:
        pass

    def _gl_handle_rejoin(self, user: User, player_state: BasePlayerState) -> None:
        pass

    def _gl_handle_play(self, user: User, data: ImmutableMultiDict) -> None:
        action = data.get('action')
        if action is None:
            raise BadUserRequestError('no action')

        user_is_initiator = user == self.initiator

        if action == 'start':
            if self.game_state != BingoGameState.STARTING:
                raise BadUserRequestError('invalid action - cannot start when not in STARTING state')
            if not user_is_initiator:
                raise BadUserRequestError('invalid action - user is not the match initiator')

            self.modify(set__game_state=BingoGameState.PLAYING)
            self._emit_ws_state_change()

        elif action == 'mark':
            if self.game_state != BingoGameState.PLAYING:
                raise BadUserRequestError('invalid action - cannot mark a spot when not in PLAYING state')

            row_no = data.get('row')
            if row_no is None:
                raise BadUserRequestError('invalid data (row_no)')

            column_no = data.get('column')
            if column_no is None:
                raise BadUserRequestError('invalid data (column_no)')

            player_state = self.get_player_state(user)

            try:
                number = player_state.card[int(row_no)][int(column_no)]
                already_marked = player_state.marked[int(row_no)][int(column_no)]
            except ValueError or IndexError:
                raise BadUserRequestError('invalid data (spot n/a)')

            is_eligible = number in self.call_numbers[0:self.current_round_number]

            if number is not None and not already_marked and is_eligible:
                player_state.marked[row_no][column_no] = True
                player_state.save()
            # else: silently fail to avoid disturbing the game flow

            self._emit_ws_state_change(user, player_state)
            self._emit_ws_player_change()

        elif action == 'win':
            player_state = self.get_player_state(user)

            if self.__get_bingo_service().check_win(player_state.card, player_state.marked):
                self.modify(set__winner=user, set__game_state=BingoGameState.ROUND_DONE)
                self._emit_ws_state_change()

        elif action == 'next_number':
            if not user_is_initiator:
                raise BadUserRequestError('invalid action - user is not the match initiator')

            self._advance_round_number()
            self._emit_ws_state_change()

        elif action == 'next_round':
            if not user_is_initiator:
                raise BadUserRequestError('invalid action - user is not the match initiator')

            for ps in self.player_states:
                self.__init_player_state(ps, True)

            self.modify(
                set__current_round_number=1, set__game_state=BingoGameState.PLAYING, set__winner=None,
                set__call_numbers=shuffle_call_numbers(self.__get_bingo_service().get_max_number())
            )
            self._emit_ws_state_change()

        else:
            raise BadUserRequestError(f'bad action `{action}`')
    # endregion

    # region View Renderers
    def _gl_render_inner_view_for_state(self, user: User, player_state: BasePlayerState = None) -> Union[str, dict]:
        user_is_initiator = user == self.initiator

        if self.game_state == BingoGameState.STARTING:
            return self._render_base_state_template(
                'starting', user, user_is_initiator=user_is_initiator
            )

        elif self.game_state == BingoGameState.PLAYING:
            if player_state is None:
                player_state = self._get_player_state(user)
            bingo_service = self.__get_bingo_service()

            current_number_int = self.call_numbers[min(bingo_service.get_max_number(), self.current_round_number) - 1]
            current_number = self.numbers[current_number_int - 1]
            called_numbers = self.call_numbers[0:max(0, self.current_round_number - 1)]  # from previous rounds

            return self._render_state_template(
                'playing', user, user_is_initiator=user_is_initiator, player_state=player_state,
                current_number=current_number, called_numbers=called_numbers,
                bingo_eligable=bingo_service.check_win(player_state.card, player_state.marked),
                next_round_eligable=self.current_round_number < bingo_service.get_max_number()
            )

        elif self.game_state == BingoGameState.ROUND_DONE:
            return self._render_state_template(
                'done', user, user_is_initiator=user_is_initiator,
                winner=self.winner, winner_ps=self.get_player_state(self.winner)
            )

        else:
            raise RuntimeError(f'cannot render state `{self.game_state}`')

    def _gl_get_player_tags(self, user: User) -> [str]:
        return []

    def get_current_points(self, user: User) -> int:
        ps = self.get_player_state(user)
        return sum(1 if rc else 0 for r in ps.marked for rc in r)
    # endregion

    # region Helpers
    def __get_bingo_service(self) -> AbstractBingoService:
        # PyCharm is too confused by this construct
        # noinspection PyTypeChecker
        return {
            BingoType.UK: UkBingoService,
            BingoType.US: UsBingoService
        }[self.type]
    # endregion
