import os
import random

import yaml

from .model.number import Number

SHRUG = '\u00AF\\_(\u30C4)_/\u00AF'


def list_numbers(max_number: int) -> [Number]:
    folder = os.path.dirname(__file__)
    file_name = os.path.join(folder, "number_data.yaml")
    with open(file_name) as f:
        yaml_contents = yaml.load(f, Loader=yaml.SafeLoader)

    numbers_dict = dict()
    yaml_numbers = yaml_contents["numbers"]
    for yaml_number in yaml_numbers:
        try:
            number = Number(
                number=yaml_number["number"],
                description=yaml_number["desc"],
                explanation=yaml_number["explanation"],
            )
        except KeyError:
            raise Exception("The yaml file contains an invalid number. "
                            "Every number must have the fields 'number', 'desc' and 'explanation'. "
                            f"The erroneous entry is this one: {yaml_number}")
        numbers_dict[number.number] = number

    return [
        numbers_dict.get(i, Number(number=i, description=SHRUG, explanation=SHRUG))
        for i in range(1, max_number + 1)
    ]


def shuffle_call_numbers(max_number: int) -> [int]:
    return random.sample(range(1, max_number + 1), max_number)
