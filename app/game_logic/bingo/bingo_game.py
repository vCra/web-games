from flask_babel import _
from werkzeug.datastructures import ImmutableMultiDict

from app.game_logic.bingo.model.bingo_type import BingoType
from app.model.user import User
from app.util.error.bad_user_request_error import BadUserRequestError
from . import number_service
from .bingo_match import BingoMatch
from .uk_bingo_service import UkBingoService
from .us_bingo_service import UsBingoService
from ..base.base_game import BaseGame


class BingoGame(BaseGame):
    @classmethod
    def get_internal_name(cls) -> str:
        return 'bingo'

    @classmethod
    def get_long_name(cls) -> str:
        return 'Bingo'

    @classmethod
    def render_view_new(cls, user: User):
        return cls._render_game_template('new', user=user)

    @classmethod
    def init_new_match(cls, data: ImmutableMultiDict, initiator: User):
        selected_bingo_type = data.get('type', 'us')
        if selected_bingo_type not in ['uk', 'us']:
            raise BadUserRequestError(_('invalid_type_selected'))
        bingo_type = {
            'uk': BingoType.UK,
            'us': BingoType.US
        }[selected_bingo_type]
        bingo_service = {
            'uk': UkBingoService,
            'us': UsBingoService
        }[selected_bingo_type]

        numbers = number_service.list_numbers(bingo_service.get_max_number())

        return BingoMatch.init_new_match(
            initiator,
            type=bingo_type,
            numbers=numbers
        )
