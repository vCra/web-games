import logging
from abc import abstractmethod
from datetime import datetime
from typing import Union

from flask import render_template, request
from flask_babel import force_locale
from flask_mongoengine import Document
from flask_socketio import emit
from mongoengine import StringField, ReferenceField, ListField, IntField, GenericReferenceField, DateTimeField
from werkzeug.datastructures import ImmutableMultiDict

from app.game_logic.base.model.base_player_state import BasePlayerState
from app.model.player_result import PlayerResult
from app.model.user import User
from app.util.id_generator import generate_id
from app.util.mongoengine_fields import StringEnumField
from .abstract_base import AbstractBase
from .model.base_state_enum import BaseGameState

logger = logging.getLogger(__name__)


class BaseMatch(AbstractBase, Document):
    """
    Abstract base class for Matches.

    A Match is an instance of a Game, while a Game is a factory for a Match.
    The Match takes care of handling events during the play time and rendering the resulting states.

    All methods prefixed with _gl_ are to be implemented by the actual Match class for handling Game Loop logic.
    """

    # region Fields
    match_id = StringField(required=True)
    initiator = ReferenceField(User, required=True)
    players = ListField(ReferenceField(User))
    results = ListField(ReferenceField(PlayerResult))
    game_state = StringEnumField(BaseGameState, required=True)
    player_states = ListField(GenericReferenceField())
    current_round_number = IntField(min_value=1, required=True)
    created = DateTimeField(default=datetime.utcnow)

    meta = {
        'allow_inheritance': True,
        'cascade': True,
        'indexes': [
            {'fields': ['created'], 'cls': False, 'expireAfterSeconds': 48 * 60 * 60}
        ]
    }
    # endregion

    # region Constructor
    @classmethod
    def init_new_match(cls, initiator: User, **kwargs):
        match = cls()

        match.initiator = initiator
        match.match_id = generate_id(initiator.discord_id)
        match.current_round_number = 1
        match.results = []

        match._gl_init(**kwargs)

        match.save()
        return match

    @abstractmethod
    def _gl_init(self, **kwargs) -> None:
        pass
    # endregion

    # region Event Handlers
    def handle_join(self, user: User) -> None:
        sid = request.sid

        player_state = self.get_player_state(user)
        if player_state is not None:
            # = player already joined earlier, bring their state up to speed

            player_state.modify(set__sid=sid)

            player_result = self.get_player_result(user)
            for _ in range(len(player_result.points_history), self.current_round_number):
                player_result.modify(push__points_history=player_result.points_history[-1])

            if not self.get_is_current_player(user):
                self.modify(push__players=user)

            self._gl_handle_rejoin(user, player_state)

        else:
            # = player joined completely fresh

            points_history = [0] * self.current_round_number
            # ^ round 0 is required to have a starting point in the result graph and to avoid extra handling for
            #    joining in round 1
            results = PlayerResult(player=user, points_history=points_history)

            results.save()
            self.modify(push__results=results)

            player_state = self._gl_handle_join_init_player_state(user)
            player_state.player = user
            player_state.sid = sid

            player_state.save()
            self.modify(push__player_states=player_state)

            self.modify(push__players=user)

            self._gl_handle_join(user)

        self._emit_ws_player_change()
        self._emit_ws_state_change(user, player_state)

    @abstractmethod
    def _gl_handle_join_init_player_state(self, user: User) -> Document:
        pass

    @abstractmethod
    def _gl_handle_join(self, user: User) -> None:
        pass

    @abstractmethod
    def _gl_handle_rejoin(self, user: User, player_state: BasePlayerState) -> None:
        pass

    def handle_play(self, user: User, data: ImmutableMultiDict) -> None:
        if not self.get_is_current_player(user):
            self.handle_join(user)

        self._gl_handle_play(user, data)

    @abstractmethod
    def _gl_handle_play(self, user: User, data: ImmutableMultiDict) -> None:
        pass

    def handle_disconnect(self, user: User) -> None:
        self.modify(pull__players=user)
        self.save()
        self._emit_ws_player_change()

        self._gl_handle_disconnect(user)

    @abstractmethod
    def _gl_handle_disconnect(self, user: User) -> None:
        pass
    # endregion

    # region View Renderers
    def render_initial_page(self, user: User) -> str:
        return self._render_game_template('base', match=self, user=user)

    def render_inner_view_for_player_list(self) -> str:
        return render_template('games/player-list.jinja2', match=self)

    def render_inner_view_for_state(self, user: User, player_state: BasePlayerState = None) -> Union[str, dict]:
        if not self.get_is_current_player(user):
            self.handle_join(user)

        return self._gl_render_inner_view_for_state(user, player_state)

    @abstractmethod
    def _gl_render_inner_view_for_state(self, user: User, player_state: BasePlayerState = None) -> Union[str, dict]:
        pass
    # endregion

    # region State Getters
    def get_current_points(self, user: User) -> int:
        matching_results = [r for r in self.results if r.player.discord_id == user.discord_id]
        try:
            return matching_results.pop().points_history.pop()
        except IndexError:  # empty list
            return 0

    def get_is_current_player(self, user: User) -> bool:
        return user.discord_id in [p.discord_id for p in self.players]

    def get_player_state(self, user: User) -> Union[BasePlayerState, None]:
        return next(iter(ps for ps in self.player_states if ps.player == user), None)

    def get_player_result(self, user: User) -> Union[BasePlayerState, None]:
        return next(iter(r for r in self.results if r.player == user), None)

    def get_player_tags(self, user: User) -> [str]:
        """
        Collect what tags should be displayed for each player in the view's player-list.
        :return: A list of icon names, with the 'las la-' prefix truncated.
        """
        return self._gl_get_player_tags(user)

    @abstractmethod
    def _gl_get_player_tags(self, user: User) -> [str]:
        pass

    def get_has_finished(self):
        return self.game_state == BaseGameState.FINISHED
    # endregion

    # region Internal Helpers
    def _render_state_template(self, relative_name: str, user: User, **kwargs):
        # Babel uses the locale set in the request context - i.e. every user gets the language of whoever triggered
        #  the state change by default. By explicitly setting the user for whom the state gets rendered we can over-
        #  write the locale for each render to fix this.
        with force_locale(user.locale):
            return super()._render_game_template(f'states/{relative_name}', user=user, match=self, **kwargs)

    def _render_base_state_template(self, relative_name: str, user: User, **kwargs):
        with force_locale(user.locale):
            return render_template(f'games/base-states/{relative_name}.jinja2', user=user, match=self, **kwargs)

    def _get_player_state(self, user: User):
        return next(iter(s for s in self.player_states if s.player == user), None)

    def __emit_ws_event(self, name: str, data: dict = None, sid: str = None) -> None:
        if data is None:
            data = {}

        if sid is None:
            for player_state in self.player_states:
                emit(name, data, namespace='/match', room=player_state.sid)
                logger.info(f'emit {name} for {player_state.sid}')
        else:
            emit(name, data, namespace='/match', room=sid)
            logger.info(f'emit {name} for {sid}')

    def _emit_ws_state_change(self, user: User = None, player_state: BasePlayerState = None):
        if user is None:
            for player_state in self.player_states:
                data = {
                    'view': self.render_inner_view_for_state(player_state.player)
                }
                self.__emit_ws_event('state_change', data, player_state.sid)

        else:
            if player_state is None:
                player_state = next(ps for ps in self.player_states if ps.player == user)
            data = {
                'view': self.render_inner_view_for_state(user, player_state)
            }
            sid = player_state.sid
            self.__emit_ws_event('state_change', data, sid)

    def _emit_ws_player_change(self):
        data = {
            'view': self.render_inner_view_for_player_list()
        }
        self.__emit_ws_event('players_change', data)

    def _emit_ws_finished(self):
        self.__emit_ws_event('finished')

    def _advance_round_number(self):
        self.modify(inc__current_round_number=1)
    # endregion
