from enum import Enum


class BaseGameState(Enum):
    STARTING = 'STARTING'
    FINISHED = 'FINISHED'
