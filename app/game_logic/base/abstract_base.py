from abc import abstractmethod

from flask import render_template


class AbstractBase:
    @classmethod
    @abstractmethod
    def get_internal_name(cls) -> str:
        pass

    @classmethod
    def _render_game_template(cls, relative_name: str, **kwargs):
        return render_template(f'games/{cls.get_internal_name()}/{relative_name}.jinja2', cls=cls, **kwargs)
