from abc import abstractmethod

from werkzeug.datastructures import ImmutableMultiDict

from app.model.user import User
from .abstract_base import AbstractBase


class BaseGame(AbstractBase):
    """
    Abstract Base Class for Games.

    A Game is a factory for Matches, while a Match is an instance of a Game.
    The Game class takes care of static properties, initial setup and parameter validation.
    """

    @classmethod
    def get_supported_locales(cls) -> [str]:
        return ['en', 'de']

    @classmethod
    @abstractmethod
    def get_long_name(cls) -> str:
        pass

    @classmethod
    @abstractmethod
    def render_view_new(cls, user: User):
        pass

    @classmethod
    @abstractmethod
    def init_new_match(cls, data: ImmutableMultiDict, initiator: User):
        pass
