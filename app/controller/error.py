import logging

from flask import render_template, Flask, request
from flask_babel import _

from app.util.error.bad_user_request_error import BadUserRequestError


def config_error_pages(app: Flask):
    def error_500(e) -> (str, int):
        try:
            from sentry_sdk import capture_exception
            capture_exception(e)
            logging.warning(f'logged error from request handling to sentry, was: {str(e)}')

        except ModuleNotFoundError:
            logging.error('unhandled error during request handling:')
            logging.exception(e)

        return (
            render_template(
                'error.jinja2',
                code='500',
                msg=_('internal_error'),
                msg_internal=str(e),
                solutions={
                    _('back_to_start'): '/'
                }
            ),
            500
        )

    app.register_error_handler(500, error_500)

    def error_404(_e) -> (str, int):
        return (
            render_template(
                'error.jinja2',
                code='404',
                msg=_('page_not_found'),
                solutions={
                    _('back_to_start'): '/'
                }
            ),
            404
        )

    app.register_error_handler(404, error_404)

    def error_bad_request(e: BadUserRequestError) -> (str, int):
        return (
            render_template(
                'error.jinja2',
                code='',
                msg=e.msg,
                msg_internal=e.details,
                solutions={
                    _('back_to_start'): '/',
                    _('change_input'): request.url
                }
            ),
            400
        )

    app.register_error_handler(BadUserRequestError, error_bad_request)


def config_ws_error_logging(socketio_app):
    def error_ws(e):
        try:
            from sentry_sdk import capture_exception
            capture_exception(e)
            logging.warning(f'logged error from WS handling to sentry, was: {str(e)}')

        except ModuleNotFoundError:
            logging.error('error during WS handling:')
            logging.exception(e)

    socketio_app.default_exception_handler = error_ws
