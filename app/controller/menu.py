from flask import render_template, request, url_for
from werkzeug.utils import redirect

import app.game_logic.class_lists as cl
from app.model.user import User
from app.util.user_guard import ensure_user_is_ready_for_http
from . import match as match_controller


@ensure_user_is_ready_for_http
def menu_home(user: User):#, settings: Settings):
    return render_template(
        'home.jinja2',
        user=user,
        owner_discord_id='asdf',#settings.get('owner_discord_id'),
        games=cl.get_supported_game_classes(user.locale)
    )


@ensure_user_is_ready_for_http
def menu_new_game(user: User, internal_game_name: str):
    game = cl.find_game_class(internal_game_name)
    return game.render_view_new(user)


@ensure_user_is_ready_for_http
def menu_start_game_match(user: User, internal_game_name: str):
    game = cl.find_game_class(internal_game_name)
    match = game.init_new_match(request.form, user)
    match.save()
    return redirect(url_for(match_controller.match_state.__name__, match_id=match.match_id))
