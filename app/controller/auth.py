from random import choice

from flask import url_for, session
from flask_dance.contrib.discord import discord, make_discord_blueprint
from mongoengine import DoesNotExist
from werkzeug.utils import redirect

from app.model.user import User
from app.settings.settings_resolver import Settings
from app.util.color import available_colors
from app.util.common_constants import SESSION_DISCORD_ID_KEY
from app.util.locale import get_locale_from_request

from . import profile as profile_controller


def config_discord_dance(settings: Settings, app):
    blueprint = make_discord_blueprint(
        client_id=settings.get('discord_auth.client_id'),
        client_secret=settings.get('discord_auth.client_secret'),
        scope='identify',
        redirect_to=auth_construct_user.__name__
    )
    app.register_blueprint(blueprint)


def auth_construct_user():
    if not discord.authorized:
        return redirect(url_for('discord.login'))

    # query data from Discord API:
    discord_resp = discord.get('/api/users/@me')
    if not discord_resp.ok:
        raise RuntimeError('discord/@me -> ko')
    discord_data = discord_resp.json()

    # determine id
    discord_id = discord_data.get('id')
    if discord_id is None:
        raise RuntimeError('discord/@me -> no id')
    session[SESSION_DISCORD_ID_KEY] = discord_id

    # determine name
    name = discord_data.get('username')
    if name is None:
        raise RuntimeError('discord/@me -> no name')

    # determine avatar - see https://discordapp.com/developers/docs/reference#image-formatting-cdn-endpoints
    avatar_hash = discord_data.get('avatar')
    if avatar_hash is None:
        # fallback default avatar
        discriminator = int(discord_data.get('discriminator')) % 5
        avatar_url = f'https://cdn.discordapp.com/embed/avatars/{discriminator}.png'
    else:
        # get custom avatar
        extension = 'gif' if avatar_hash.startswith('a_') else 'png'
        avatar_url = f'https://cdn.discordapp.com/avatars/{discord_id}/{avatar_hash}.{extension}'

    # construct user
    try:
        # try to read from DB, if the user exists we want to keep the existing custom values from the DB
        user = User.objects.get(discord_id=discord_id)
    except DoesNotExist:
        # if not saved yet, use new values
        new_color = choice(available_colors)
        new_locale = get_locale_from_request()
        user = User(discord_id=discord_id, color=new_color, locale=new_locale)
    # update details provided by Discord
    user.name = name
    user.avatar_url = avatar_url
    # persist
    user.save()

    # done, let's let the user customize the values
    return redirect(url_for(profile_controller.profile_edit.__name__))
