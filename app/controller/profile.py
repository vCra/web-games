from flask import render_template, url_for, request, session
from werkzeug.utils import redirect

from app.model.user import User
from app.util.color import available_colors, color_column_widths
from app.util.common_constants import SESSION_TARGET_AFTER_AUTH
from app.util.user_guard import ensure_user_is_ready_for_http

from . import menu as menu_controller


@ensure_user_is_ready_for_http
def profile_edit(user: User):
    return render_template(
        'profile.jinja2',
        user=user, colors=available_colors, color_column_widths=color_column_widths
    )


@ensure_user_is_ready_for_http
def profile_save(user: User):
    user.color = request.form.get('color', '#FFFFFF')
    user.locale = request.form.get('locale', 'en')
    user.save()

    target = session.get(SESSION_TARGET_AFTER_AUTH)
    if target is not None:
        session[SESSION_TARGET_AFTER_AUTH] = None
        return redirect(target)
    else:
        return redirect(url_for(menu_controller.menu_home.__name__))


def profile_logout():
    session.clear()
    return redirect(url_for(menu_controller.menu_home.__name__))
