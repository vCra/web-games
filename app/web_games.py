import logging
import random

from flask import Flask, send_from_directory, request, render_template, session
from flask_babel import Babel
from flask_mongoengine import MongoEngine
from flask_socketio import SocketIO

import app._version
from .controller import checks, auth, profile, menu, match, error
from .settings.settings_resolver import Settings
from .util.common_constants import SESSION_DISCORD_ID_KEY
from .util.error.user_not_found_error import UserNotFoundError
from .util.locale import get_locale_from_request
from .util.user_guard import get_user_from_session

"""
All the glue code lives here
"""

# region Various Setup
logging.basicConfig(format='%(asctime)s | %(name)s | %(levelname)s: %(message)s', level=logging.INFO)
logging.getLogger('engineio').setLevel(logging.WARNING)

logging.info('Starting, setting up config...')

random.seed()

settings = Settings()

try:
    import sentry_sdk
    from sentry_sdk.integrations.flask import FlaskIntegration
    sentry_sdk.init(
        dsn=settings.get('sentry_dsn'),
        integrations=[FlaskIntegration()],
        release=app._version.__version__
    )

    logging.info('configured Sentry')
except ModuleNotFoundError:
    logging.warning("couldn't configure Sentry - sentry_sdk not installed")
except BaseException as ex:
    logging.warning(f"couldn't configure Sentry - other error: {str(ex)}")

app = Flask(__name__, static_folder='static')
app.secret_key = settings.get('secret_key')

app.add_url_rule('/favicon.ico', view_func=lambda: send_from_directory('static', filename='media/favicon.ico'))


app.config['MONGODB_SETTINGS'] = settings.get('mongo')
db = MongoEngine(app)


babel = Babel(app)
@babel.localeselector
def get_locale():
    try:
        return get_user_from_session().locale
    except UserNotFoundError:
        return get_locale_from_request()


@app.before_request
def log_request_info():
    logging.info(
        f'got request - url={request.url} session.discord_id={session.get(SESSION_DISCORD_ID_KEY, "n/a")}'
    )


@app.before_request
def redirect_bots():
    user_agent = request.headers.get('User-Agent')
    if user_agent is None or 'bot' in user_agent.lower():
        return render_template('bot.jinja2')


socketio_app = SocketIO(
    app, ping_timeout=settings.get('ws_ping.timeout'), ping_interval=settings.get('ws_ping.interval')
)
# endregion


# region Controller-Mapping

# -- Health Check
app.add_url_rule('/health/', view_func=checks.health_check)

# -- Error Handling
error.config_error_pages(app)
error.config_ws_error_logging(socketio_app)

# -- Auth:
# = '/discord'
# = '/discord/authorized'
auth.config_discord_dance(settings, app)
app.add_url_rule('/auth/', view_func=auth.auth_construct_user)

# --- User-Details:
app.add_url_rule('/profile/', view_func=profile.profile_edit)
app.add_url_rule('/profile/logout/', view_func=profile.profile_logout)
app.add_url_rule('/profile/', methods=['POST'], view_func=profile.profile_save)

# --- Menu:
app.add_url_rule('/', view_func=menu.menu_home)
app.add_url_rule('/new-game/<internal_game_name>', view_func=menu.menu_new_game)
app.add_url_rule('/new-game/<internal_game_name>', methods=['POST'], view_func=menu.menu_start_game_match)

# --- Match:
app.add_url_rule('/match/<match_id>', view_func=match.match_state)
app.add_url_rule('/match/<match_id>/results', view_func=match.match_result)

socketio_app.on_event('join', namespace='/match', handler=match.match_ws_join)
socketio_app.on_event('play', namespace='/match', handler=match.match_ws_play)
socketio_app.on_event('disconnect', namespace='/match', handler=match.match_ws_disconnect)
# endregion
