## Deployment Setup for OpenShift

based upon [Painless Continuous Delivery](https://github.com/painless-software/painless-continuous-delivery/)


### Pre-Requisites
- create a project on OpenShift console
    - resource limits: 250m CPU, 256MB RAM
- add a service account for GitLab CI to push images:
    1. login to OpenShift cluster
    1. create the service account:
        ```sh
        oc -n ${TARGET_NAMESPACE} create sa gitlab-ci
        oc -n ${TARGET_NAMESPACE} policy add-role-to-user admin -z gitlab-ci
        oc -n ${TARGET_NAMESPACE} sa get-token gitlab-ci
        ```
    1. open the GitLab repo, select **Operations** and **Kubernetes** in the side-bar
    1. add a new existing cluster, with the following values:
        - **Environment scope**: `*`
        - **API URL**: `https://console.appuio.ch`
        - **Service Token**: the value printed by get-token earlier
        - **RBAC-enabled cluster**: on
        - **GitLab-managed cluster**: off
- add a config-map for the settings
    - **name**: `web-games-settings`
    - **key**: `settings`
    - **value**: copy-paste of settings.yaml

### Reference

initial config for Painless Continuous Delivery (although this setup is pretty widely customized by now):
```
cookiecutter gh:painless-software/painless-continuous-delivery \
    project_name="web-games" \
    project_description="play some games" \
    vcs_platform="GitLab.com" \
    vcs_account="LucaVazz" \
    vcs_project="web-games" \
    ci_service=".gitlab-ci.yml" \
    cloud_platform="APPUiO" \
    cloud_account="hey@example.com" \
    cloud_project="luva-main" \
    environment_strategy="shared" \
    docker_registry="registry.appuio.ch" \
    framework="Flask" \
    database="(none)" \
    cronjobs="(none)" \
    checks="kubernetes" \
    tests="py37" \
    monitoring="(none)" \
    license="GPL-3" \
    --no-input
```
