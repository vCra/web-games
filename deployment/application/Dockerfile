# -- STAGE 1 - Node env for frontend dependencies

# ENV SETUP:
FROM node:12-alpine as static

WORKDIR /temp
COPY . .

# INSTALL DEPENDENCIES:
RUN yarn
RUN yarn clean



# -- STAGE 2 - Python env for the app

# ENV SETUP:
FROM python:3.8-alpine

ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    PYTHONUNBUFFERED=1


# INJECT APP-FILES:
WORKDIR /app

COPY . .


# INSTALL DEPENDENCIES:
RUN apk add build-base
RUN pip --disable-pip-version-check --no-cache-dir \
    install -r requirements-prod.txt


# COMPILE RUNTIME-DEPENDENCIES:
RUN sh ./scripts/babel_compile.sh


# INJECT FRONTEND DEPENDENCIES:
COPY --from=static /temp/app/static /app/app/static


# FINISH CONTAINER SETUP:
# support arbitrary user IDs (OpenShift guidelines)
RUN chown -R 1001:0 /app /run \
 && chmod -R g=u    /app /run /etc/passwd
USER 1001:0

CMD ["python3", "/app/run-server.py"]
