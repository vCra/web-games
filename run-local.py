from app.web_games import app, socketio_app


socketio_app.run(app, ssl_context=('ssl.cert', 'ssl.key'))
